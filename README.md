# Service

## Запуск из образов на Docker Hub
Для запуска сервиса из образа размещенного на Docker Hub перейдите в папку docker-hub-image и выполните следующую команду:
   ```shell
      ansible-playbook <path-to>/docker-service.yaml --extra-vars "image_name=<dcker hub image name> image_version=<image version> app_port=<app port inside docker>"
   ``` 

## Запуск из git-репозитория с Docker Compose
Для запуска сервиса из исходников Docker Compose размещённых в git-репозитории переййдите в папку git-docker-compose выполните следующую команду:
   ```shell
      ansible-playbook <path-to>/docker-compose-service.yaml --extra-vars "git_repositiry=<https git repository address>"
   ``` 
или для конкретного коммита:
   ```shell
      ansible-playbook <path-to>/docker-compose-service.yaml --extra-vars "git_repositiry=<https git repository address> git_commit=<commit hash>"
   ``` 